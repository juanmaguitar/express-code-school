var express = require('express');
var app = express(); // application instance
var logger = require('./logger');
var blocks = require('./routes/blocks');
var locations = require('./routes/locations');
var port, ip;

app.use( logger );
app.use( express.static('public') );

app.use( '/blocks', blocks );
app.use( '/locations', locations );

port = process.env.PORT;
ip = process.env.IP;

app.listen(process.env.PORT, process.env.IP,function(){
  console.log("Listening on port "+ip+":"+port+"...");
});