## Launching 

Load the app by doing

    node app.js
    
or

    nodemon app.js

to reload automatically after changes in the code

## Preview

Once the node app is executed the files are served in the URL
    
    https://express-code-school-juanma.c9users.io/
