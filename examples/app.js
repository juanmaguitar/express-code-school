var express = require('express')
var app = express(); // application instance

app.get('/', function(request, response){
  response.send('Hello world');
  response.end();
})

app.get('/blocks', function(request, response){
  var blocks = ['Fixed','Movable','Rotating'];
  var htmlBlocks = '<ul><li>Fixed</li><li>Movable</li></ul>'
  //response.send(blocks);
  //response.json(blocks);
  response.send(htmlBlocks);
})

app.get('/parts', function(request, response){
  response.redirect(301,'/more-arts')
})

var port = process.env.PORT;
var ip = process.env.IP;

app.listen(process.env.PORT, process.env.IP,function(){
  console.log("Listening on port "+ip+":"+port+"...");
});