var express = require('express');
var router = express.Router();
var filterName = require('../filters/name');

var locations = {
  'Fixed' : 'First floor',
  'Movable' : 'Second floor',
  'Rotating' : 'Penthouse'
}

router.route('/:name')
    .all( filterName )
    .get( function(request, response){
        var location = locations[request.blockName];
        if (!location) {
            response.status(404).json('No description found for ' + request.params.name);
        }
        else {
            response.json(location);
        }
    })

module.exports = router;