var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({extended: false});
var filterName = require('../filters/name');

var blocks = {
  'Fixed' : 'Fastened securely in a position',
  'Movable' : 'Capable of being moved',
  'Rotating' : 'Moving in a circle around its center'
};

router.route('/')
    .post( parseUrlencoded, function(request, response){   // POST /blocks
        var newBlock = request.body;
        blocks[newBlock.name] = newBlock.description;
        response.status(201).json(newBlock.name);
    })
    .get( function(request, response){   // GET /blocks
        var blocksNames = Object.keys(blocks); 
        if (request.query.limit >= 0) {
          response.json(blocksNames.slice(0,request.query.limit));  
        }
        else {
          response.json(blocksNames);  
        }
    });

router.route('/:name')
    .all( filterName )
    .get( function(request, response){
        var description = blocks[request.blockName];
        if (!description) {
          response.status(404).json('No description found for ' + request.params.name);
        }
        else {
          response.json(description);
        }
    } )
    .delete( function(request, response){
        var block = request.blockName;
        if (blocks[block]){
          delete blocks[block];
          response.sendStatus(200);
        }
        else {
          response.sendStatus(400);
        }
    } )

module.exports = router;